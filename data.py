from datetime import datetime

def init_db(mi_conexion):

    cursor=mi_conexion.cursor()
    cursor.execute(""" CREATE TABLE IF NOT EXISTS users (
        persona_id INTEGER PRIMARY KEY AUTOINCREMENT
        usuario text primary key,
        nombre text not null,
        contraseña text not null
    );""")

    cursor.execute(""" CREATE TABLE IF NOT EXISTS recognitions (
        usuario text not null,
        hora text not null,
        foreign key(usuario) references users(usuario)
    );""")

    mi_conexion.commit()


def insert_user(con, nombre,usuario,password):
    cursor1 = con.cursor()
    cursor1.execute('INSERT INTO users(nombre, usuario, contraseña) values(?,?,?)', (nombre, usuario, password))
    con.commit()


def user_recognized(con, name):
    cursor = con.cursor()

    sql = "SELECT hora FROM recognitions WHERE usuario=? AND hora >= datetime('now', '-10 minutes')"
    cursor.execute(sql, (name,))
    row = cursor.fetchone()

    if row is None or row[0] is not None:
        sql = "INSERT INTO recognitions (usuario, hora) VALUES (?, ?)"
        val = (name, datetime.now())
        cursor.execute(sql, val)
    

    con.commit()