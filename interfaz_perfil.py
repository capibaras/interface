import tkinter as tk
from PIL import Image, ImageTk
import cv2
import imutils
import os
from data import insert_user
from tkinter import filedialog
from tkinter import messagebox
import time
from recognition import FaceRecognition
from recognition import face_confidence
import sqlite3
from tkinter import ttk


raiz=tk.Tk()
raiz.title("Identificación de usuarios")
raiz.geometry("650x350")
raiz.resizable(False, False)
raiz.config(bg="DarkOliveGreen3")
video = None
con = sqlite3.connect("./src/database/test.db")


def video_reconocimiento():
    if __name__ == '__main__':
        fr = FaceRecognition()
        fr.run_recognition()


def ventana_register():
    ventana = tk.Toplevel(raiz)
    ventana.title("Register")
    ventana.config(bg="SteelBlue2")
    ventana.resizable(True, True)
    ventana.geometry("1080x720")

    # NOMBRE Y APELLIDO
    etiq_nombre = tk.Label(ventana, text="Nombre y apellido:", bg="SteelBlue2")
    etiq_nombre.place(x="10", y="50")
    nombre_entry = tk.Entry(ventana, width=30)
    nombre_entry.place(x=155, y=50)
    nombre_entry.config(width=30)

    # USUARIO
    etiq_usuario = tk.Label(ventana, text="Usuario:", bg="SteelBlue2")
    etiq_usuario.place(x="10", y="75")
    usuario_entry = tk.Entry(ventana)
    usuario_entry.place(x=155, y=75)
    usuario_entry.config(width=30)

    # PASSWORD
    etiq_password = tk.Label(ventana, text="Contraseña:", bg="SteelBlue2")
    etiq_password.place(x="10", y="100")
    password_entry = tk.Entry(ventana, width=30, show="*")
    password_entry.place(x=155, y=100)
    password_entry.config(width=30)


    def comparar():


        archivos = os.listdir("./faces")
        diferenciar = usuario_entry.get() + ".png"
        for archivo in archivos:
            if archivo == diferenciar:
                mensaje_error = tk.Label(ventana, text= usuario_entry.get() + " ya es un usuario registrado")
                mensaje_error.place(x="650", y="525")
                mensaje_error.config(bg="white", fg="red", font=("Serif", 10, "bold"))
                def eliminar_mensaje():
                    mensaje_error.destroy()                
                ventana.after(5000, eliminar_mensaje)
                return
                        
        def capturar_imagen():
            captura = cv2.VideoCapture(0)
            ret, imagen = captura.read()
            captura.release()
            
            cascada_cara = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')
            caras = cascada_cara.detectMultiScale(imagen, scaleFactor=1.1, minNeighbors=5, minSize=(30, 30))
            if len(caras) > 0:
                insert_user(con, nombre_entry.get(), usuario_entry.get(), password_entry.get())
                imagen_capturada = cv2.cvtColor(imagen, cv2.COLOR_BGR2RGB)
                ruta_guardado = "./faces/"
                nombre_archivo = usuario_entry.get() + ".png"
                ruta_completa = os.path.join(ruta_guardado, nombre_archivo)
                cv2.imwrite(ruta_completa, imagen_capturada)
                mostrar_imagen(ruta_completa)
            else:
                tk.messagebox.showwarning(parent=ventana,title="Advertencia", message="No se encontró ningún rostro en la imagen capturada.")
                return

    
        def mostrar_imagen(ruta_completa):
            image = Image.open(ruta_completa)
            tk_image = ImageTk.PhotoImage(image)
            etiqueta_imagen = tk.Label(ventana, image=tk_image)
            etiqueta_imagen.configure(width=640, height=480)
            etiqueta_imagen.image = tk_image
            etiqueta_imagen.place(x=450, y=15)
            etiq_usuario = tk.Label(ventana, text= usuario_entry.get() + " ha sido registrado correctamente !")
            etiq_usuario.place(x="650", y="525")
            etiq_usuario.config(bg="white", fg="green", font=("Serif", 10, "bold"))
    
            def eliminar_usuario():
                etiq_usuario.destroy()                
            ventana.after(5000, eliminar_usuario)
        capturar_imagen()
        mostrar_imagen()
    #Boton añadir
    boton_ventana_añadir = tk.Button(ventana, text="Añadir usuario", command=comparar)
    boton_ventana_añadir.place(x="155", y="200")
    boton_ventana_añadir.config(cursor="hand2", bg="grey", relief="flat", width=10, height=1, font=("Calisto MT", 12, "bold"))


    ventana.mainloop()


def ventana_perfil():
    ventana_perfil = tk.Toplevel(raiz)
    ventana_perfil.title("Perfil")
    ventana_perfil.config(bg="SteelBlue2")
    ventana_perfil.resizable(False, False)
    ventana_perfil.geometry("650x350")

    # USUARIO
    etiq_usuario_perfil = tk.Label(ventana_perfil, text="Usuario", bg="SteelBlue2")
    etiq_usuario_perfil.place(x=310, y=55)
    usuario_perfil_entry = tk.Entry(ventana_perfil, justify="center")
    usuario_perfil_entry.place(x=215, y=75)
    usuario_perfil_entry.config(width=30)

    # PASSWORD
    etiq_password_perfil = tk.Label(ventana_perfil, text="Contraseña", bg="SteelBlue2")
    etiq_password_perfil.place(x=300, y=130)
    password_perfil_entry = tk.Entry(ventana_perfil, width=30, show="*", justify="center")
    password_perfil_entry.place(x=215, y=150)
    password_perfil_entry.config(width=30)


    #BOTONES y sus FUNCIONES

    def limpiar():
        usuario_perfil_entry.delete(0, tk.END)
        password_perfil_entry.delete(0, tk.END)

    def login():
        username = usuario_perfil_entry.get()
        password = password_perfil_entry.get()

        con = sqlite3.connect("./src/database/test.db")
        cursor = con.cursor()
        cursor.execute("SELECT * FROM users WHERE usuario=? AND contraseña=?", (username, password))
        result = cursor.fetchone()
        

        if result:
            user = result[0]
            cursor.execute("SELECT u.nombre, u.usuario, r.hora FROM users u INNER JOIN recognitions r ON u.usuario = r.usuario WHERE u.usuario=?", (user,))
            data = cursor.fetchall()
            con.close()
            if data:
                subventana_perfil = tk.Toplevel(ventana_perfil)
                subventana_perfil.title(f"Bienvenido, {result[1]}!")
                subventana_perfil.config(bg="SteelBlue2")
                subventana_perfil.resizable(False, False)
                subventana_perfil.geometry("600x325")

                # Crear tabla
                table = ttk.Treeview(subventana_perfil)
                table["columns"] = ["nombre", "usuario", "hora"]
                table.column("#0", width=0, stretch=tk.NO)
                table.column("nombre", anchor=tk.CENTER, width=150, stretch=False)
                table.column("usuario", anchor=tk.CENTER, width=150, stretch=False)
                table.column("hora", anchor=tk.CENTER, width=200, stretch=False)
                table.place(x=50 , y=50)

                table.heading("#0", text="")
                table.heading("nombre", text="Nombre")
                table.heading("usuario", text="Usuario")
                table.heading("hora", text="Hora")

                # Insertar filas en la tabla
                for row in data:
                    table.insert("", tk.END, text="", values=(row[0], row[1], row[2]))

                
                subventana_perfil.mainloop()
            else:
                tk.messagebox.showwarning(parent=ventana_perfil, title="Advertencia", message="No hay datos relacionados con el usuario " + result[0])
        
        else:
            tk.messagebox.showerror(parent=ventana_perfil, title="Error", message="Nombre de usuario o contraseña incorrectos")



    boton_perfil_añadir = tk.Button(ventana_perfil, text="LIMPIAR", command=limpiar)
    boton_perfil_añadir.place(x=215, y=200)
    boton_perfil_añadir.config(cursor="hand2", bg="grey", relief="flat", width=8, height=1, font=("Calisto MT", 12, "bold"))

    boton_perfil2_añadir = tk.Button(ventana_perfil, text="ENTRAR", command=login)
    boton_perfil2_añadir.place(x=345, y=200)
    boton_perfil2_añadir.config(cursor="hand2", bg="grey", relief="flat", width=8, height=1, font=("Calisto MT", 12, "bold"))







#ICONOS

imagen = Image.open("./icono/perfil.ico")
imagen = imagen.resize((50, 50), Image.ANTIALIAS)
imagen_tk = ImageTk.PhotoImage(imagen)

boton_perfil = tk.Button(raiz, width=50, height=50)
boton_perfil.place(x="570", y="25")
boton_perfil.config(cursor="hand2", bg="gray62", font=("Serif", 12, "bold"), image=imagen_tk, command=ventana_perfil)

#-------------------------------------------------------------------------------------------#

#BOTONES
boton_registrarse = tk.Button(raiz, text="REGISTRARSE", command=ventana_register, width=15, height=1)
boton_registrarse.place(x="250", y="175")
boton_registrarse.config(cursor="hand2", bg="gray62", font=("Serif", 12, "bold") )

boton_video = tk.Button(raiz, text="VIDEO", command=video_reconocimiento, width=15, height=1)
boton_video.place(x="250", y="125")
boton_video.config(cursor="hand2", bg="gray62", font=("Serif", 12, "bold"))
   

raiz.mainloop()